       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA1.
       AUTHOR. SIRARAT
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 NUM1            PIC   999   VALUE ZEROS.
       01 NUM2            PIC   999   VALUE 15.
       01 TAX-RATE        PIC   V99   VALUE .35.
       01 CUSTOMER-NAME   PIC   X(15) VALUE "Sirarat".

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "NUM " NUM1
           DISPLAY "NUM " NUM2
           DISPLAY "TAX-RATE "   TAX-RATE
           DISPLAY "CUSTOMER " CUSTOMER-NAME
           .